import java.io.Serializable;
import streamit.library.*;
import streamit.library.io.*;
import streamit.misc.StreamItRandom;
class Complex extends Structure implements Serializable {
  float real;
  float imag;
}
class float2 extends Structure implements Serializable {
  float x;
  float y;
}
class float3 extends Structure implements Serializable {
  float x;
  float y;
  float z;
}
class float4 extends Structure implements Serializable {
  float x;
  float y;
  float z;
  float w;
}
class StreamItVectorLib {
  public static native float2 add2(float2 a, float2 b);
  public static native float3 add3(float3 a, float3 b);
  public static native float4 add4(float4 a, float4 b);
  public static native float2 sub2(float2 a, float2 b);
  public static native float3 sub3(float3 a, float3 b);
  public static native float4 sub4(float4 a, float4 b);
  public static native float2 mul2(float2 a, float2 b);
  public static native float3 mul3(float3 a, float3 b);
  public static native float4 mul4(float4 a, float4 b);
  public static native float2 div2(float2 a, float2 b);
  public static native float3 div3(float3 a, float3 b);
  public static native float4 div4(float4 a, float4 b);
  public static native float2 addScalar2(float2 a, float b);
  public static native float3 addScalar3(float3 a, float b);
  public static native float4 addScalar4(float4 a, float b);
  public static native float2 subScalar2(float2 a, float b);
  public static native float3 subScalar3(float3 a, float b);
  public static native float4 subScalar4(float4 a, float b);
  public static native float2 scale2(float2 a, float b);
  public static native float3 scale3(float3 a, float b);
  public static native float4 scale4(float4 a, float b);
  public static native float2 scaleInv2(float2 a, float b);
  public static native float3 scaleInv3(float3 a, float b);
  public static native float4 scaleInv4(float4 a, float b);
  public static native float sqrtDist2(float2 a, float2 b);
  public static native float sqrtDist3(float3 a, float3 b);
  public static native float sqrtDist4(float4 a, float4 b);
  public static native float dot3(float3 a, float3 b);
  public static native float3 cross3(float3 a, float3 b);
  public static native float2 max2(float2 a, float2 b);
  public static native float3 max3(float3 a, float3 b);
  public static native float2 min2(float2 a, float2 b);
  public static native float3 min3(float3 a, float3 b);
  public static native float2 neg2(float2 a);
  public static native float3 neg3(float3 a);
  public static native float4 neg4(float4 a);
  public static native float2 floor2(float2 a);
  public static native float3 floor3(float3 a);
  public static native float4 floor4(float4 a);
  public static native float2 normalize2(float2 a);
  public static native float3 normalize3(float3 a);
  public static native float4 normalize4(float4 a);
  public static native boolean greaterThan3(float3 a, float3 b);
  public static native boolean lessThan3(float3 a, float3 b);
  public static native boolean equals3(float3 a, float3 b);
}
class source extends Filter // forw.str:25
{
  public source()
  {
  }
  public void work() { // forw.str:27
    float[4][3] A; // forw.str:29
    A[0][0] = 0; // forw.str:30
    A[0][1] = 0; // forw.str:31
    A[0][2] = 0; // forw.str:32
    A[1][0] = 0; // forw.str:33
    A[1][1] = 1; // forw.str:34
    A[1][2] = 2; // forw.str:35
    A[2][0] = 0; // forw.str:36
    A[2][1] = 2; // forw.str:37
    A[2][2] = 5; // forw.str:38
    A[3][0] = 0; // forw.str:39
    A[3][1] = 4; // forw.str:40
    A[3][2] = 2; // forw.str:41
    float[3][2] B; // forw.str:43
    B[0][0] = 1; // forw.str:45
    B[0][1] = 2; // forw.str:46
    B[1][0] = 2; // forw.str:47
    B[1][1] = 3; // forw.str:48
    B[2][0] = 4; // forw.str:49
    B[2][1] = 2; // forw.str:50
    for (int i = 0; (i < 4); i++) { // forw.str:53
      for (int j = 0; (j < 3); j++) { // forw.str:54
        outputChannel.pushFloat(A[i][j]); // forw.str:54
      }; // forw.str:53
    }; // forw.str:52
    for (int i = 0; (i < 3); i++) { // forw.str:57
      for (int j = 0; (j < 2); j++) { // forw.str:58
        outputChannel.pushFloat(B[i][j]); // forw.str:58
      }; // forw.str:57
    }; // forw.str:56
  }
  public void init() { // forw.str:25
    setIOTypes(Void.TYPE, Float.TYPE); // forw.str:25
    addSteadyPhase(0, 0, 18, "work"); // forw.str:27
  }
}
class sink extends Filter // forw.str:64
{
  public sink(int _param_M)
  {
  }
  int M; // forw.str:64
  public void work() { // forw.str:65
    for (int i = 0; (i < M); i++) { // forw.str:67
      System.out.println(inputChannel.popFloat()); // forw.str:67
    }; // forw.str:66
  }
  public void init(final int _param_M) { // forw.str:64
    M = _param_M; // forw.str:64
    setIOTypes(Float.TYPE, Void.TYPE); // forw.str:64
    addSteadyPhase(M, M, 0, "work"); // forw.str:65
  }
}
class mpcore extends Filter // forw.str:82
{
  public mpcore(int _param_M, int _param_N, int _param_H, int _param_W)
  {
  }
  float[M][N] A; // forw.str:84
  float[H][W] B; // forw.str:87
  int M; // forw.str:82
  int N; // forw.str:82
  int H; // forw.str:82
  int W; // forw.str:82
  public void work() { // forw.str:89
    int x; // forw.str:91
    int y; // forw.str:92
    float ediff; // forw.str:93
    float temp1; // forw.str:94
    float temp2; // forw.str:96
    x = 0; // forw.str:98
    y = 0; // forw.str:99
    ediff = 0; // forw.str:100
    for (int i = 0; (i < M); i++) { // forw.str:103
      for (int j = 0; (j < N); j++) { // forw.str:104
        A[i][j] = inputChannel.popFloat(); // forw.str:104
      }; // forw.str:103
    }; // forw.str:102
    for (int i = 0; (i < H); i++) { // forw.str:107
      for (int j = 0; (j < W); j++) { // forw.str:108
        B[i][j] = inputChannel.popFloat(); // forw.str:108
      }; // forw.str:107
    }; // forw.str:106
    ediff = ((M * N) * 256); // forw.str:110
    for (int i = 0; (i <= (M - H)); i++) { // forw.str:114
      for (int j = 0; (j <= (N - W)); j++) { // forw.str:115
        temp1 = 0; // forw.str:116
        for (int k = 0; (k < H); k++) { // forw.str:118
          for (int l = 0; (l < W); l++) { // forw.str:119
            temp1 += ((B[k][l] - A[(k + i)][(j + l)]) * (B[k][l] - A[(k + i)][(j + l)])); // forw.str:119
          }; // forw.str:118
        }; // forw.str:117
        if ((temp1 < ediff)) { // forw.str:121
          x = i; // forw.str:122
          y = j; // forw.str:123
          ediff = temp1; // forw.str:124
        } // forw.str:121
      }; // forw.str:114
    }; // forw.str:113
    outputChannel.pushFloat(x); // forw.str:128
    outputChannel.pushFloat(y); // forw.str:129
    for (int i = 0; (i < H); i++) { // forw.str:132
      for (int j = 0; (j < W); j++) { // forw.str:133
        outputChannel.pushFloat((B[i][j] - A[(x + i)][(y + j)])); // forw.str:133
      }; // forw.str:132
    }; // forw.str:131
  }
  public void init(final int _param_M, final int _param_N, final int _param_H, final int _param_W) { // forw.str:82
    M = _param_M; // forw.str:82
    N = _param_N; // forw.str:82
    H = _param_H; // forw.str:82
    W = _param_W; // forw.str:82
    setIOTypes(Float.TYPE, Float.TYPE); // forw.str:82
    addSteadyPhase(((M * N) + (H * W)), ((M * N) + (H * W)), (2 + (H * W)), "work"); // forw.str:89
  }
}
public class forw extends StreamItPipeline // forw.str:15
{
  public void init() { // forw.str:15
    add(new source()); // forw.str:16
    add(new mpcore(4, 3, 3, 2)); // forw.str:17
    add(new sink((2 + 6))); // forw.str:18
  }
}
