#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <message.h>
#include <netsocket.h>
#include <node_server.h>
#include <init_instance.h>
#include <master_server.h>
#include <save_state.h>
#include <save_manager.h>
#include <delete_chkpts.h>
#include <object_write_buffer.h>
#include <read_setup.h>
#include <ccp.h>
#include <timer.h>
#include <streamit_random.h>
#include "structs.h"
#include "fusion.h"

int __max_iteration;
int __timer_enabled = 0;
int __frequency_of_chkpts;
volatile int __vol;
proc_timer tt("total runtime");


float BUFFER_0_1[__BUF_SIZE_MASK_0_1 + 1];
int HEAD_0_1 = 0;
int TAIL_0_1 = 0;
float BUFFER_1_2[__BUF_SIZE_MASK_1_2 + 1];
int HEAD_1_2 = 0;
int TAIL_1_2 = 0;
void init_source__10_4__0();
void work_source__10_4__0(int);
void init_mpcore__32_5__1();
void work_mpcore__32_5__1(int);
void init_sink__37_6__2();
void work_sink__37_6__2(int);

int main(int argc, char **argv) {
  read_setup::read_setup_file();
  __max_iteration = read_setup::max_iteration;
  for (int a = 1; a < argc; a++) {
    if (argc > a + 1 && strcmp(argv[a], "-i") == 0) {
      int tmp;
      sscanf(argv[a + 1], "%d", &tmp);
#ifdef VERBOSE
      fprintf(stderr,"Number of Iterations: %d\n", tmp);
#endif
      __max_iteration = tmp;
    }
    if (strcmp(argv[a], "-t") == 0) {
#ifdef VERBOSE
      fprintf(stderr,"Timer enabled.\n");
#endif
      __timer_enabled = 1;
    }
  }
// number of phases: 3


  // ============= Initialization =============

init_source__10_4__0();
init_mpcore__32_5__1();
init_sink__37_6__2();

  // ============= Steady State =============

  if (__timer_enabled) {
    tt.start();
  }
  for (int n = 0; n < (__max_iteration  ); n++) {
HEAD_0_1 = 0;
TAIL_0_1 = 0;
    work_source__10_4__0(1 );
HEAD_1_2 = 0;
TAIL_1_2 = 0;
    work_mpcore__32_5__1(1 );
    work_sink__37_6__2(1 );
  }
if (__timer_enabled) {
    tt.stop();
    tt.output(stderr);
  }


  return 0;
}

// moved or inserted by concat_cluster_threads.pl
message *__msg_stack_1;
message *__msg_stack_0;
message *__msg_stack_2;

// end of moved or inserted by concat_cluster_threads.pl

// peek: 0 pop: 0 push 18
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false


#include <mysocket.h>
#include <sdep.h>
#include <thread_info.h>
#include <consumer2.h>
#include <consumer2p.h>
#include <producer2.h>
#include "cluster.h"
#include "global.h"

int __number_of_iterations_0;
int __counter_0 = 0;
int __steady_0 = 0;
int __tmp_0 = 0;
int __tmp2_0 = 0;
int *__state_flag_0 = NULL;
thread_info *__thread_0 = NULL;



void save_file_pointer__0(object_write_buffer *buf);
void load_file_pointer__0(object_write_buffer *buf);

inline void check_status__0() {
  check_thread_status(__state_flag_0, __thread_0);
}

void check_status_during_io__0() {
  check_thread_status_during_io(__state_flag_0, __thread_0);
}

void __init_thread_info_0(thread_info *info) {
  __state_flag_0 = info->get_state_flag();
}

thread_info *__get_thread_info_0() {
  if (__thread_0 != NULL) return __thread_0;
  __thread_0 = new thread_info(0, check_status_during_io__0);
  __init_thread_info_0(__thread_0);
  return __thread_0;
}

void __declare_sockets_0() {
  init_instance::add_outgoing(0,1, DATA_SOCKET);
}

void __init_sockets_0(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_0() {
}

void __peek_sockets_0() {
}

 
void init_source__10_4__0();
inline void check_status__0();

void work_source__10_4__0(int);



inline void __push_0_1(float data) {
BUFFER_0_1[HEAD_0_1]=data;
HEAD_0_1++;
}



 
void init_source__10_4__0(){
}
void save_file_pointer__0(object_write_buffer *buf) {}
void load_file_pointer__0(object_write_buffer *buf) {}
 
void work_source__10_4__0(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float A__2[4][3] = {0};/* float[4][3] */
      float B__3[3][2] = {0};/* float[3][2] */
      float __tmp2__4 = 0.0f;/* float */
      int j__conflict__1__5 = 0;/* int */
      int i__conflict__0__6 = 0;/* int */
      float __tmp3__7 = 0.0f;/* float */
      int j__8 = 0;/* int */
      int i__9 = 0;/* int */

      // mark begin: SIRFilter source

      (((A__2[(int)0])[(int)0]) = ((float)0.0))/*float*/;
      (((A__2[(int)0])[(int)1]) = ((float)0.0))/*float*/;
      (((A__2[(int)0])[(int)2]) = ((float)0.0))/*float*/;
      (((A__2[(int)1])[(int)0]) = ((float)0.0))/*float*/;
      (((A__2[(int)1])[(int)1]) = ((float)1.0))/*float*/;
      (((A__2[(int)1])[(int)2]) = ((float)2.0))/*float*/;
      (((A__2[(int)2])[(int)0]) = ((float)0.0))/*float*/;
      (((A__2[(int)2])[(int)1]) = ((float)2.0))/*float*/;
      (((A__2[(int)2])[(int)2]) = ((float)5.0))/*float*/;
      (((A__2[(int)3])[(int)0]) = ((float)0.0))/*float*/;
      (((A__2[(int)3])[(int)1]) = ((float)4.0))/*float*/;
      (((A__2[(int)3])[(int)2]) = ((float)2.0))/*float*/;
      (((B__3[(int)0])[(int)0]) = ((float)1.0))/*float*/;
      (((B__3[(int)0])[(int)1]) = ((float)2.0))/*float*/;
      (((B__3[(int)1])[(int)0]) = ((float)2.0))/*float*/;
      (((B__3[(int)1])[(int)1]) = ((float)3.0))/*float*/;
      (((B__3[(int)2])[(int)0]) = ((float)4.0))/*float*/;
      (((B__3[(int)2])[(int)1]) = ((float)2.0))/*float*/;
      for ((i__conflict__0__6 = 0)/*int*/; (i__conflict__0__6 < 4); (i__conflict__0__6++)) {for ((j__conflict__1__5 = 0)/*int*/; (j__conflict__1__5 < 3); (j__conflict__1__5++)) {{
            (__tmp2__4 = ((A__2[(int)i__conflict__0__6])[(int)j__conflict__1__5]))/*float*/;
            __push_0_1(__tmp2__4);
          }
        }
      }
      for ((i__9 = 0)/*int*/; (i__9 < 3); (i__9++)) {for ((j__8 = 0)/*int*/; (j__8 < 2); (j__8++)) {{
            (__tmp3__7 = ((B__3[(int)i__9])[(int)j__8]))/*float*/;
            __push_0_1(__tmp3__7);
          }
        }
      }
      // mark end: SIRFilter source

    }
  }
}

// peek: 18 pop: 18 push 8
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_1;
int __counter_1 = 0;
int __steady_1 = 0;
int __tmp_1 = 0;
int __tmp2_1 = 0;
int *__state_flag_1 = NULL;
thread_info *__thread_1 = NULL;



float A__11__1[4][3] = {0};
float B__12__1[3][2] = {0};
void save_peek_buffer__1(object_write_buffer *buf);
void load_peek_buffer__1(object_write_buffer *buf);
void save_file_pointer__1(object_write_buffer *buf);
void load_file_pointer__1(object_write_buffer *buf);

inline void check_status__1() {
  check_thread_status(__state_flag_1, __thread_1);
}

void check_status_during_io__1() {
  check_thread_status_during_io(__state_flag_1, __thread_1);
}

void __init_thread_info_1(thread_info *info) {
  __state_flag_1 = info->get_state_flag();
}

thread_info *__get_thread_info_1() {
  if (__thread_1 != NULL) return __thread_1;
  __thread_1 = new thread_info(1, check_status_during_io__1);
  __init_thread_info_1(__thread_1);
  return __thread_1;
}

void __declare_sockets_1() {
  init_instance::add_incoming(0,1, DATA_SOCKET);
  init_instance::add_outgoing(1,2, DATA_SOCKET);
}

void __init_sockets_1(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_1() {
}

void __peek_sockets_1() {
}

 
void init_mpcore__32_5__1();
inline void check_status__1();

void work_mpcore__32_5__1(int);


inline float __pop_0_1() {
float res=BUFFER_0_1[TAIL_0_1];
TAIL_0_1++;
return res;
}

inline void __pop_0_1(int n) {
TAIL_0_1+=n;
}

inline float __peek_0_1(int offs) {
return BUFFER_0_1[TAIL_0_1+offs];
}



inline void __push_1_2(float data) {
BUFFER_1_2[HEAD_1_2]=data;
HEAD_1_2++;
}



 
void init_mpcore__32_5__1(){
}
void save_file_pointer__1(object_write_buffer *buf) {}
void load_file_pointer__1(object_write_buffer *buf) {}
 
void work_mpcore__32_5__1(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int x__15 = 0;/* int */
      int y__16 = 0;/* int */
      float ediff__17 = 0.0f;/* float */
      float temp1__18 = 0.0f;/* float */
      int j__conflict__5__19 = 0;/* int */
      int i__conflict__4__20 = 0;/* int */
      int j__conflict__3__21 = 0;/* int */
      int i__conflict__2__22 = 0;/* int */
      int l__23 = 0;/* int */
      int k__24 = 0;/* int */
      int j__conflict__1__25 = 0;/* int */
      int i__conflict__0__26 = 0;/* int */
      float __tmp4__27 = 0.0f;/* float */
      float __tmp5__28 = 0.0f;/* float */
      float __tmp6__29 = 0.0f;/* float */
      int j__30 = 0;/* int */
      int i__31 = 0;/* int */

      // mark begin: SIRFilter mpcore

      (x__15 = 0)/*int*/;
      (y__16 = 0)/*int*/;
      (ediff__17 = ((float)0.0))/*float*/;
      for ((i__conflict__4__20 = 0)/*int*/; (i__conflict__4__20 < 4); (i__conflict__4__20++)) {for ((j__conflict__5__19 = 0)/*int*/; (j__conflict__5__19 < 3); (j__conflict__5__19++)) {(((A__11__1)[(int)i__conflict__4__20])[(int)j__conflict__5__19]) = BUFFER_0_1[TAIL_0_1]; TAIL_0_1++;
;
        }
      }
      for ((i__conflict__2__22 = 0)/*int*/; (i__conflict__2__22 < 3); (i__conflict__2__22++)) {for ((j__conflict__3__21 = 0)/*int*/; (j__conflict__3__21 < 2); (j__conflict__3__21++)) {(((B__12__1)[(int)i__conflict__2__22])[(int)j__conflict__3__21]) = BUFFER_0_1[TAIL_0_1]; TAIL_0_1++;
;
        }
      }
      (ediff__17 = ((float)3072.0))/*float*/;
      for ((i__conflict__0__26 = 0)/*int*/; (i__conflict__0__26 <= 1); (i__conflict__0__26++)) {for ((j__conflict__1__25 = 0)/*int*/; (j__conflict__1__25 <= 1); (j__conflict__1__25++)) {{
            (temp1__18 = ((float)0.0))/*float*/;
            for ((k__24 = 0)/*int*/; (k__24 < 3); (k__24++)) {for ((l__23 = 0)/*int*/; (l__23 < 2); (l__23++)) {(temp1__18 = (temp1__18 + (((((B__12__1)[(int)k__24])[(int)l__23]) - (((A__11__1)[(int)(k__24 + i__conflict__0__26)])[(int)(j__conflict__1__25 + l__23)])) * ((((B__12__1)[(int)k__24])[(int)l__23]) - (((A__11__1)[(int)(k__24 + i__conflict__0__26)])[(int)(j__conflict__1__25 + l__23)])))))/*float*/;
              }
            }
            if ((temp1__18 < ediff__17)) {{
              (x__15 = i__conflict__0__26)/*int*/;
              (y__16 = j__conflict__1__25)/*int*/;
              (ediff__17 = temp1__18)/*float*/;
            }
            } else {}
          }
        }
      }
      (__tmp4__27 = ((float)(x__15)))/*float*/;
      __push_1_2(__tmp4__27);
      (__tmp5__28 = ((float)(y__16)))/*float*/;
      __push_1_2(__tmp5__28);
      for ((i__31 = 0)/*int*/; (i__31 < 3); (i__31++)) {for ((j__30 = 0)/*int*/; (j__30 < 2); (j__30++)) {{
            (__tmp6__29 = ((((B__12__1)[(int)i__31])[(int)j__30]) - (((A__11__1)[(int)(x__15 + i__31)])[(int)(y__16 + j__30)])))/*float*/;
            __push_1_2(__tmp6__29);
          }
        }
      }
      // mark end: SIRFilter mpcore

    }
  }
}

// peek: 8 pop: 8 push 0
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_2;
int __counter_2 = 0;
int __steady_2 = 0;
int __tmp_2 = 0;
int __tmp2_2 = 0;
int *__state_flag_2 = NULL;
thread_info *__thread_2 = NULL;



void save_peek_buffer__2(object_write_buffer *buf);
void load_peek_buffer__2(object_write_buffer *buf);
void save_file_pointer__2(object_write_buffer *buf);
void load_file_pointer__2(object_write_buffer *buf);

inline void check_status__2() {
  check_thread_status(__state_flag_2, __thread_2);
}

void check_status_during_io__2() {
  check_thread_status_during_io(__state_flag_2, __thread_2);
}

void __init_thread_info_2(thread_info *info) {
  __state_flag_2 = info->get_state_flag();
}

thread_info *__get_thread_info_2() {
  if (__thread_2 != NULL) return __thread_2;
  __thread_2 = new thread_info(2, check_status_during_io__2);
  __init_thread_info_2(__thread_2);
  return __thread_2;
}

void __declare_sockets_2() {
  init_instance::add_incoming(1,2, DATA_SOCKET);
}

void __init_sockets_2(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_2() {
}

void __peek_sockets_2() {
}

 
void init_sink__37_6__2();
inline void check_status__2();

void work_sink__37_6__2(int);


inline float __pop_1_2() {
float res=BUFFER_1_2[TAIL_1_2];
TAIL_1_2++;
return res;
}

inline void __pop_1_2(int n) {
TAIL_1_2+=n;
}

inline float __peek_1_2(int offs) {
return BUFFER_1_2[TAIL_1_2+offs];
}


 
void init_sink__37_6__2(){
}
void save_file_pointer__2(object_write_buffer *buf) {}
void load_file_pointer__2(object_write_buffer *buf) {}
 
void work_sink__37_6__2(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp7__35 = 0.0f;/* float */
      int i__36 = 0;/* int */

      // mark begin: SIRFilter sink

      for ((i__36 = 0)/*int*/; (i__36 < 8); (i__36++)) {{
          __tmp7__35 = BUFFER_1_2[TAIL_1_2]; TAIL_1_2++;
;

          // TIMER_PRINT_CODE: __print_sink__ += (int)(__tmp7__35); 
          printf( "%f", __tmp7__35); printf("\n");

        }
      }
      // mark end: SIRFilter sink

    }
  }
}

