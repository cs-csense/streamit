// peek: 18 pop: 18 push 8
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false

#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <init_instance.h>
#include <mysocket.h>
#include <object_write_buffer.h>
#include <save_state.h>
#include <sdep.h>
#include <message.h>
#include <timer.h>
#include <thread_info.h>
#include <consumer2.h>
#include <consumer2p.h>
#include <producer2.h>
#include "cluster.h"
#include "fusion.h"
#include "global.h"

extern int __max_iteration;
extern int __init_iter;
extern int __timer_enabled;
extern int __frequency_of_chkpts;
extern volatile int __vol;
message *__msg_stack_1;
int __number_of_iterations_1;
int __counter_1 = 0;
int __steady_1 = 0;
int __tmp_1 = 0;
int __tmp2_1 = 0;
int *__state_flag_1 = NULL;
thread_info *__thread_1 = NULL;



float A__11__1[4][3] = {0};
float B__12__1[3][2] = {0};
void save_peek_buffer__1(object_write_buffer *buf);
void load_peek_buffer__1(object_write_buffer *buf);
void save_file_pointer__1(object_write_buffer *buf);
void load_file_pointer__1(object_write_buffer *buf);

inline void check_status__1() {
  check_thread_status(__state_flag_1, __thread_1);
}

void check_status_during_io__1() {
  check_thread_status_during_io(__state_flag_1, __thread_1);
}

void __init_thread_info_1(thread_info *info) {
  __state_flag_1 = info->get_state_flag();
}

thread_info *__get_thread_info_1() {
  if (__thread_1 != NULL) return __thread_1;
  __thread_1 = new thread_info(1, check_status_during_io__1);
  __init_thread_info_1(__thread_1);
  return __thread_1;
}

void __declare_sockets_1() {
  init_instance::add_incoming(0,1, DATA_SOCKET);
  init_instance::add_outgoing(1,2, DATA_SOCKET);
}

void __init_sockets_1(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_1() {
}

void __peek_sockets_1() {
}

 
void init_mpcore__32_5__1();
inline void check_status__1();

void work_mpcore__32_5__1(int);

extern float BUFFER_0_1[];
extern int HEAD_0_1;
extern int TAIL_0_1;

inline float __pop_0_1() {
float res=BUFFER_0_1[TAIL_0_1];
TAIL_0_1++;
return res;
}

inline void __pop_0_1(int n) {
TAIL_0_1+=n;
}

inline float __peek_0_1(int offs) {
return BUFFER_0_1[TAIL_0_1+offs];
}

extern float BUFFER_1_2[];
extern int HEAD_1_2;
extern int TAIL_1_2;


inline void __push_1_2(float data) {
BUFFER_1_2[HEAD_1_2]=data;
HEAD_1_2++;
}



 
void init_mpcore__32_5__1(){
}
void save_file_pointer__1(object_write_buffer *buf) {}
void load_file_pointer__1(object_write_buffer *buf) {}
 
void work_mpcore__32_5__1(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int x__15 = 0;/* int */
      int y__16 = 0;/* int */
      float ediff__17 = 0.0f;/* float */
      float temp1__18 = 0.0f;/* float */
      int j__conflict__5__19 = 0;/* int */
      int i__conflict__4__20 = 0;/* int */
      int j__conflict__3__21 = 0;/* int */
      int i__conflict__2__22 = 0;/* int */
      int l__23 = 0;/* int */
      int k__24 = 0;/* int */
      int j__conflict__1__25 = 0;/* int */
      int i__conflict__0__26 = 0;/* int */
      float __tmp4__27 = 0.0f;/* float */
      float __tmp5__28 = 0.0f;/* float */
      float __tmp6__29 = 0.0f;/* float */
      int j__30 = 0;/* int */
      int i__31 = 0;/* int */

      // mark begin: SIRFilter mpcore

      (x__15 = 0)/*int*/;
      (y__16 = 0)/*int*/;
      (ediff__17 = ((float)0.0))/*float*/;
      for ((i__conflict__4__20 = 0)/*int*/; (i__conflict__4__20 < 4); (i__conflict__4__20++)) {for ((j__conflict__5__19 = 0)/*int*/; (j__conflict__5__19 < 3); (j__conflict__5__19++)) {(((A__11__1)[(int)i__conflict__4__20])[(int)j__conflict__5__19]) = BUFFER_0_1[TAIL_0_1]; TAIL_0_1++;
;
        }
      }
      for ((i__conflict__2__22 = 0)/*int*/; (i__conflict__2__22 < 3); (i__conflict__2__22++)) {for ((j__conflict__3__21 = 0)/*int*/; (j__conflict__3__21 < 2); (j__conflict__3__21++)) {(((B__12__1)[(int)i__conflict__2__22])[(int)j__conflict__3__21]) = BUFFER_0_1[TAIL_0_1]; TAIL_0_1++;
;
        }
      }
      (ediff__17 = ((float)3072.0))/*float*/;
      for ((i__conflict__0__26 = 0)/*int*/; (i__conflict__0__26 <= 1); (i__conflict__0__26++)) {for ((j__conflict__1__25 = 0)/*int*/; (j__conflict__1__25 <= 1); (j__conflict__1__25++)) {{
            (temp1__18 = ((float)0.0))/*float*/;
            for ((k__24 = 0)/*int*/; (k__24 < 3); (k__24++)) {for ((l__23 = 0)/*int*/; (l__23 < 2); (l__23++)) {(temp1__18 = (temp1__18 + (((((B__12__1)[(int)k__24])[(int)l__23]) - (((A__11__1)[(int)(k__24 + i__conflict__0__26)])[(int)(j__conflict__1__25 + l__23)])) * ((((B__12__1)[(int)k__24])[(int)l__23]) - (((A__11__1)[(int)(k__24 + i__conflict__0__26)])[(int)(j__conflict__1__25 + l__23)])))))/*float*/;
              }
            }
            if ((temp1__18 < ediff__17)) {{
              (x__15 = i__conflict__0__26)/*int*/;
              (y__16 = j__conflict__1__25)/*int*/;
              (ediff__17 = temp1__18)/*float*/;
            }
            } else {}
          }
        }
      }
      (__tmp4__27 = ((float)(x__15)))/*float*/;
      __push_1_2(__tmp4__27);
      (__tmp5__28 = ((float)(y__16)))/*float*/;
      __push_1_2(__tmp5__28);
      for ((i__31 = 0)/*int*/; (i__31 < 3); (i__31++)) {for ((j__30 = 0)/*int*/; (j__30 < 2); (j__30++)) {{
            (__tmp6__29 = ((((B__12__1)[(int)i__31])[(int)j__30]) - (((A__11__1)[(int)(x__15 + i__31)])[(int)(y__16 + j__30)])))/*float*/;
            __push_1_2(__tmp6__29);
          }
        }
      }
      // mark end: SIRFilter mpcore

    }
  }
}

