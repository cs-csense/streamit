// peek: 2 pop: 2 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false

#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <init_instance.h>
#include <mysocket.h>
#include <object_write_buffer.h>
#include <save_state.h>
#include <sdep.h>
#include <message.h>
#include <timer.h>
#include <thread_info.h>
#include <consumer2.h>
#include <consumer2p.h>
#include <producer2.h>
#include "cluster.h"
#include "fusion.h"
#include "global.h"

extern int __max_iteration;
extern int __init_iter;
extern int __timer_enabled;
extern int __frequency_of_chkpts;
extern volatile int __vol;
message *__msg_stack_3;
int __number_of_iterations_3;
int __counter_3 = 0;
int __steady_3 = 0;
int __tmp_3 = 0;
int __tmp2_3 = 0;
int *__state_flag_3 = NULL;
thread_info *__thread_3 = NULL;



void save_peek_buffer__3(object_write_buffer *buf);
void load_peek_buffer__3(object_write_buffer *buf);
void save_file_pointer__3(object_write_buffer *buf);
void load_file_pointer__3(object_write_buffer *buf);

inline void check_status__3() {
  check_thread_status(__state_flag_3, __thread_3);
}

void check_status_during_io__3() {
  check_thread_status_during_io(__state_flag_3, __thread_3);
}

void __init_thread_info_3(thread_info *info) {
  __state_flag_3 = info->get_state_flag();
}

thread_info *__get_thread_info_3() {
  if (__thread_3 != NULL) return __thread_3;
  __thread_3 = new thread_info(3, check_status_during_io__3);
  __init_thread_info_3(__thread_3);
  return __thread_3;
}

void __declare_sockets_3() {
  init_instance::add_incoming(2,3, DATA_SOCKET);
  init_instance::add_outgoing(3,4, DATA_SOCKET);
}

void __init_sockets_3(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_3() {
}

void __peek_sockets_3() {
}

 
void init_VectAddKernel__15_12__3();
inline void check_status__3();

void work_VectAddKernel__15_12__3(int);

extern int BUFFER_2_3[];
extern int HEAD_2_3;
extern int TAIL_2_3;

inline int __pop_2_3() {
int res=BUFFER_2_3[TAIL_2_3];
TAIL_2_3++;
return res;
}

inline void __pop_2_3(int n) {
TAIL_2_3+=n;
}

inline int __peek_2_3(int offs) {
return BUFFER_2_3[TAIL_2_3+offs];
}

extern int BUFFER_3_4[];
extern int HEAD_3_4;
extern int TAIL_3_4;


inline void __push_3_4(int data) {
BUFFER_3_4[HEAD_3_4]=data;
HEAD_3_4++;
}



 
void init_VectAddKernel__15_12__3(){
}
void save_file_pointer__3(object_write_buffer *buf) {}
void load_file_pointer__3(object_write_buffer *buf) {}
 
void work_VectAddKernel__15_12__3(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int t1__12 = 0;/* int */
      int t2__13 = 0;/* int */
      int __tmp4__14 = 0;/* int */

      // mark begin: SIRFilter VectAddKernel

      t1__12 = BUFFER_2_3[TAIL_2_3]; TAIL_2_3++;
;
      t2__13 = BUFFER_2_3[TAIL_2_3]; TAIL_2_3++;
;
      (__tmp4__14 = (t1__12 + t2__13))/*int*/;
      __push_3_4(__tmp4__14);
      // mark end: SIRFilter VectAddKernel

    }
  }
}

