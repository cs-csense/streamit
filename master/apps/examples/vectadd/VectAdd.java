import java.io.Serializable;
import streamit.library.*;
import streamit.library.io.*;
import streamit.misc.StreamItRandom;
class Complex extends Structure implements Serializable {
  float real;
  float imag;
}
class float2 extends Structure implements Serializable {
  float x;
  float y;
}
class float3 extends Structure implements Serializable {
  float x;
  float y;
  float z;
}
class float4 extends Structure implements Serializable {
  float x;
  float y;
  float z;
  float w;
}
class StreamItVectorLib {
  public static native float2 add2(float2 a, float2 b);
  public static native float3 add3(float3 a, float3 b);
  public static native float4 add4(float4 a, float4 b);
  public static native float2 sub2(float2 a, float2 b);
  public static native float3 sub3(float3 a, float3 b);
  public static native float4 sub4(float4 a, float4 b);
  public static native float2 mul2(float2 a, float2 b);
  public static native float3 mul3(float3 a, float3 b);
  public static native float4 mul4(float4 a, float4 b);
  public static native float2 div2(float2 a, float2 b);
  public static native float3 div3(float3 a, float3 b);
  public static native float4 div4(float4 a, float4 b);
  public static native float2 addScalar2(float2 a, float b);
  public static native float3 addScalar3(float3 a, float b);
  public static native float4 addScalar4(float4 a, float b);
  public static native float2 subScalar2(float2 a, float b);
  public static native float3 subScalar3(float3 a, float b);
  public static native float4 subScalar4(float4 a, float b);
  public static native float2 scale2(float2 a, float b);
  public static native float3 scale3(float3 a, float b);
  public static native float4 scale4(float4 a, float b);
  public static native float2 scaleInv2(float2 a, float b);
  public static native float3 scaleInv3(float3 a, float b);
  public static native float4 scaleInv4(float4 a, float b);
  public static native float sqrtDist2(float2 a, float2 b);
  public static native float sqrtDist3(float3 a, float3 b);
  public static native float sqrtDist4(float4 a, float4 b);
  public static native float dot3(float3 a, float3 b);
  public static native float3 cross3(float3 a, float3 b);
  public static native float2 max2(float2 a, float2 b);
  public static native float3 max3(float3 a, float3 b);
  public static native float2 min2(float2 a, float2 b);
  public static native float3 min3(float3 a, float3 b);
  public static native float2 neg2(float2 a);
  public static native float3 neg3(float3 a);
  public static native float4 neg4(float4 a);
  public static native float2 floor2(float2 a);
  public static native float3 floor3(float3 a);
  public static native float4 floor4(float4 a);
  public static native float2 normalize2(float2 a);
  public static native float3 normalize3(float3 a);
  public static native float4 normalize4(float4 a);
  public static native boolean greaterThan3(float3 a, float3 b);
  public static native boolean lessThan3(float3 a, float3 b);
  public static native boolean equals3(float3 a, float3 b);
}
class VectAddKernel extends Filter // VectAdd.str:8
{
  public VectAddKernel()
  {
  }
  public void work() { // VectAdd.str:9
    int t1 = inputChannel.popInt(), t2 = inputChannel.popInt(); // VectAdd.str:10
    outputChannel.pushInt((t1 + t2)); // VectAdd.str:13
  }
  public void init() { // VectAdd.str:8
    setIOTypes(Integer.TYPE, Integer.TYPE); // VectAdd.str:8
    addSteadyPhase(2, 2, 1, "work"); // VectAdd.str:9
  }
}
class VectSource extends Filter // VectAdd.str:20
{
  public VectSource(int _param_N)
  {
    setStateful(true);
  }
  int idx; // VectAdd.str:21
  int N; // VectAdd.str:20
  public void work() { // VectAdd.str:22
    outputChannel.pushInt((idx++ % N)); // VectAdd.str:23
  }
  public void init(final int _param_N) { // VectAdd.str:20
    N = _param_N; // VectAdd.str:20
    setIOTypes(Void.TYPE, Integer.TYPE); // VectAdd.str:20
    addSteadyPhase(0, 0, 1, "work"); // VectAdd.str:22
  }
}
class TwoVectSource extends SplitJoin // VectAdd.str:30
{
  public TwoVectSource(int N)
  {
  }
  public void init(final int N) { // VectAdd.str:30
    setSplitter(ROUND_ROBIN(0)); // VectAdd.str:32
    add(new VectSource(N)); // VectAdd.str:33
    add(new VectSource(N)); // VectAdd.str:34
    setJoiner(ROUND_ROBIN(1)); // VectAdd.str:35
  }
}
class VectPrinter extends Filter // VectAdd.str:41
{
  public VectPrinter()
  {
  }
  public void work() { // VectAdd.str:42
    System.out.println(inputChannel.popInt()); // VectAdd.str:43
  }
  public void init() { // VectAdd.str:41
    setIOTypes(Integer.TYPE, Void.TYPE); // VectAdd.str:41
    addSteadyPhase(1, 1, 0, "work"); // VectAdd.str:42
  }
}
public class VectAdd extends StreamItPipeline // VectAdd.str:50
{
  public void init() { // VectAdd.str:50
    int N = 10; // VectAdd.str:51
    add(new TwoVectSource(N)); // VectAdd.str:63
    add(new VectAddKernel()); // VectAdd.str:65
    add(new VectPrinter()); // VectAdd.str:66
  }
}
