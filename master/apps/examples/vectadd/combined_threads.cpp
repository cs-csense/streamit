#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <message.h>
#include <netsocket.h>
#include <node_server.h>
#include <init_instance.h>
#include <master_server.h>
#include <save_state.h>
#include <save_manager.h>
#include <delete_chkpts.h>
#include <object_write_buffer.h>
#include <read_setup.h>
#include <ccp.h>
#include <timer.h>
#include <streamit_random.h>
#include "structs.h"
#include "fusion.h"

int __max_iteration;
int __timer_enabled = 0;
int __frequency_of_chkpts;
volatile int __vol;
proc_timer tt("total runtime");


int BUFFER_1_2[__BUF_SIZE_MASK_1_2 + 1];
int HEAD_1_2 = 0;
int TAIL_1_2 = 0;
int BUFFER_2_3[__BUF_SIZE_MASK_2_3 + 1];
int HEAD_2_3 = 0;
int TAIL_2_3 = 0;
int BUFFER_3_4[__BUF_SIZE_MASK_3_4 + 1];
int HEAD_3_4 = 0;
int TAIL_3_4 = 0;
int BUFFER_5_2[__BUF_SIZE_MASK_5_2 + 1];
int HEAD_5_2 = 0;
int TAIL_5_2 = 0;
void __splitter_0_work(int);
void init_VectSource__4_10__1();
void work_VectSource__4_10__1(int);
void __joiner_2_work(int);
void init_VectAddKernel__15_12__3();
void work_VectAddKernel__15_12__3(int);
void init_VectPrinter__19_13__4();
void work_VectPrinter__19_13__4(int);
void init_VectSource__9_11__5();
void work_VectSource__9_11__5(int);

int main(int argc, char **argv) {
  read_setup::read_setup_file();
  __max_iteration = read_setup::max_iteration;
  for (int a = 1; a < argc; a++) {
    if (argc > a + 1 && strcmp(argv[a], "-i") == 0) {
      int tmp;
      sscanf(argv[a + 1], "%d", &tmp);
#ifdef VERBOSE
      fprintf(stderr,"Number of Iterations: %d\n", tmp);
#endif
      __max_iteration = tmp;
    }
    if (strcmp(argv[a], "-t") == 0) {
#ifdef VERBOSE
      fprintf(stderr,"Timer enabled.\n");
#endif
      __timer_enabled = 1;
    }
  }
// number of phases: 5


  // ============= Initialization =============

init_VectSource__4_10__1();
init_VectSource__9_11__5();
init_VectAddKernel__15_12__3();
init_VectPrinter__19_13__4();

  // ============= Steady State =============

  if (__timer_enabled) {
    tt.start();
  }
  for (int n = 0; n < (__max_iteration  ); n++) {

HEAD_1_2 = 0;
TAIL_1_2 = 0;
    work_VectSource__4_10__1(1 );
HEAD_5_2 = 0;
TAIL_5_2 = 0;
    work_VectSource__9_11__5(1 );
HEAD_2_3 = 0;
TAIL_2_3 = 0;
    __joiner_2_work(1 );
HEAD_3_4 = 0;
TAIL_3_4 = 0;
    work_VectAddKernel__15_12__3(1 );
    work_VectPrinter__19_13__4(1 );
  }
if (__timer_enabled) {
    tt.stop();
    tt.output(stderr);
  }


  return 0;
}

// moved or inserted by concat_cluster_threads.pl
message *__msg_stack_4;
message *__msg_stack_1;
message *__msg_stack_3;
message *__msg_stack_5;
message *__msg_stack_2;

// end of moved or inserted by concat_cluster_threads.pl

// peek: 0 pop: 0 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false


#include <mysocket.h>
#include <sdep.h>
#include <thread_info.h>
#include <consumer2.h>
#include <consumer2p.h>
#include <producer2.h>
#include "cluster.h"
#include "global.h"

int __number_of_iterations_1;
int __counter_1 = 0;
int __steady_1 = 0;
int __tmp_1 = 0;
int __tmp2_1 = 0;
int *__state_flag_1 = NULL;
thread_info *__thread_1 = NULL;



int idx__0__1 = 0;
void save_file_pointer__1(object_write_buffer *buf);
void load_file_pointer__1(object_write_buffer *buf);

inline void check_status__1() {
  check_thread_status(__state_flag_1, __thread_1);
}

void check_status_during_io__1() {
  check_thread_status_during_io(__state_flag_1, __thread_1);
}

void __init_thread_info_1(thread_info *info) {
  __state_flag_1 = info->get_state_flag();
}

thread_info *__get_thread_info_1() {
  if (__thread_1 != NULL) return __thread_1;
  __thread_1 = new thread_info(1, check_status_during_io__1);
  __init_thread_info_1(__thread_1);
  return __thread_1;
}

void __declare_sockets_1() {
  init_instance::add_outgoing(1,2, DATA_SOCKET);
}

void __init_sockets_1(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_1() {
}

void __peek_sockets_1() {
}

 
void init_VectSource__4_10__1();
inline void check_status__1();

void work_VectSource__4_10__1(int);



inline void __push_1_2(int data) {
BUFFER_1_2[HEAD_1_2]=data;
HEAD_1_2++;
}



 
void init_VectSource__4_10__1(){
}
void save_file_pointer__1(object_write_buffer *buf) {}
void load_file_pointer__1(object_write_buffer *buf) {}
 
void work_VectSource__4_10__1(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int __tmp2__3 = 0;/* int */

      // mark begin: SIRFilter VectSource

      (__tmp2__3 = (((idx__0__1)++) % 10))/*int*/;
      __push_1_2(__tmp2__3);
      // mark end: SIRFilter VectSource

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_2;
int __counter_2 = 0;
int __steady_2 = 0;
int __tmp_2 = 0;
int __tmp2_2 = 0;
int *__state_flag_2 = NULL;
thread_info *__thread_2 = NULL;



inline void check_status__2() {
  check_thread_status(__state_flag_2, __thread_2);
}

void check_status_during_io__2() {
  check_thread_status_during_io(__state_flag_2, __thread_2);
}

void __init_thread_info_2(thread_info *info) {
  __state_flag_2 = info->get_state_flag();
}

thread_info *__get_thread_info_2() {
  if (__thread_2 != NULL) return __thread_2;
  __thread_2 = new thread_info(2, check_status_during_io__2);
  __init_thread_info_2(__thread_2);
  return __thread_2;
}

void __declare_sockets_2() {
  init_instance::add_incoming(1,2, DATA_SOCKET);
  init_instance::add_incoming(5,2, DATA_SOCKET);
  init_instance::add_outgoing(2,3, DATA_SOCKET);
}

void __init_sockets_2(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_2() {
}

void __peek_sockets_2() {
}


inline void __push_2_3(int data) {
BUFFER_2_3[HEAD_2_3]=data;
HEAD_2_3++;
}


inline int __pop_1_2() {
int res=BUFFER_1_2[TAIL_1_2];
TAIL_1_2++;
return res;
}

inline int __pop_5_2() {
int res=BUFFER_5_2[TAIL_5_2];
TAIL_5_2++;
return res;
}


void __joiner_2_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_2_3(__pop_1_2());
    __push_2_3(__pop_5_2());
  }
}


// peek: 2 pop: 2 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_3;
int __counter_3 = 0;
int __steady_3 = 0;
int __tmp_3 = 0;
int __tmp2_3 = 0;
int *__state_flag_3 = NULL;
thread_info *__thread_3 = NULL;



void save_peek_buffer__3(object_write_buffer *buf);
void load_peek_buffer__3(object_write_buffer *buf);
void save_file_pointer__3(object_write_buffer *buf);
void load_file_pointer__3(object_write_buffer *buf);

inline void check_status__3() {
  check_thread_status(__state_flag_3, __thread_3);
}

void check_status_during_io__3() {
  check_thread_status_during_io(__state_flag_3, __thread_3);
}

void __init_thread_info_3(thread_info *info) {
  __state_flag_3 = info->get_state_flag();
}

thread_info *__get_thread_info_3() {
  if (__thread_3 != NULL) return __thread_3;
  __thread_3 = new thread_info(3, check_status_during_io__3);
  __init_thread_info_3(__thread_3);
  return __thread_3;
}

void __declare_sockets_3() {
  init_instance::add_incoming(2,3, DATA_SOCKET);
  init_instance::add_outgoing(3,4, DATA_SOCKET);
}

void __init_sockets_3(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_3() {
}

void __peek_sockets_3() {
}

 
void init_VectAddKernel__15_12__3();
inline void check_status__3();

void work_VectAddKernel__15_12__3(int);


inline int __pop_2_3() {
int res=BUFFER_2_3[TAIL_2_3];
TAIL_2_3++;
return res;
}

inline void __pop_2_3(int n) {
TAIL_2_3+=n;
}

inline int __peek_2_3(int offs) {
return BUFFER_2_3[TAIL_2_3+offs];
}



inline void __push_3_4(int data) {
BUFFER_3_4[HEAD_3_4]=data;
HEAD_3_4++;
}



 
void init_VectAddKernel__15_12__3(){
}
void save_file_pointer__3(object_write_buffer *buf) {}
void load_file_pointer__3(object_write_buffer *buf) {}
 
void work_VectAddKernel__15_12__3(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int t1__12 = 0;/* int */
      int t2__13 = 0;/* int */
      int __tmp4__14 = 0;/* int */

      // mark begin: SIRFilter VectAddKernel

      t1__12 = BUFFER_2_3[TAIL_2_3]; TAIL_2_3++;
;
      t2__13 = BUFFER_2_3[TAIL_2_3]; TAIL_2_3++;
;
      (__tmp4__14 = (t1__12 + t2__13))/*int*/;
      __push_3_4(__tmp4__14);
      // mark end: SIRFilter VectAddKernel

    }
  }
}

// peek: 1 pop: 1 push 0
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_4;
int __counter_4 = 0;
int __steady_4 = 0;
int __tmp_4 = 0;
int __tmp2_4 = 0;
int *__state_flag_4 = NULL;
thread_info *__thread_4 = NULL;



void save_peek_buffer__4(object_write_buffer *buf);
void load_peek_buffer__4(object_write_buffer *buf);
void save_file_pointer__4(object_write_buffer *buf);
void load_file_pointer__4(object_write_buffer *buf);

inline void check_status__4() {
  check_thread_status(__state_flag_4, __thread_4);
}

void check_status_during_io__4() {
  check_thread_status_during_io(__state_flag_4, __thread_4);
}

void __init_thread_info_4(thread_info *info) {
  __state_flag_4 = info->get_state_flag();
}

thread_info *__get_thread_info_4() {
  if (__thread_4 != NULL) return __thread_4;
  __thread_4 = new thread_info(4, check_status_during_io__4);
  __init_thread_info_4(__thread_4);
  return __thread_4;
}

void __declare_sockets_4() {
  init_instance::add_incoming(3,4, DATA_SOCKET);
}

void __init_sockets_4(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_4() {
}

void __peek_sockets_4() {
}

 
void init_VectPrinter__19_13__4();
inline void check_status__4();

void work_VectPrinter__19_13__4(int);


inline int __pop_3_4() {
int res=BUFFER_3_4[TAIL_3_4];
TAIL_3_4++;
return res;
}

inline void __pop_3_4(int n) {
TAIL_3_4+=n;
}

inline int __peek_3_4(int offs) {
return BUFFER_3_4[TAIL_3_4+offs];
}


 
void init_VectPrinter__19_13__4(){
}
void save_file_pointer__4(object_write_buffer *buf) {}
void load_file_pointer__4(object_write_buffer *buf) {}
 
void work_VectPrinter__19_13__4(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int __tmp5__18 = 0;/* int */

      // mark begin: SIRFilter VectPrinter

      __tmp5__18 = BUFFER_3_4[TAIL_3_4]; TAIL_3_4++;
;

      // TIMER_PRINT_CODE: __print_sink__ += (int)(__tmp5__18); 
      printf( "%d", __tmp5__18); printf("\n");

      // mark end: SIRFilter VectPrinter

    }
  }
}

// peek: 0 pop: 0 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_5;
int __counter_5 = 0;
int __steady_5 = 0;
int __tmp_5 = 0;
int __tmp2_5 = 0;
int *__state_flag_5 = NULL;
thread_info *__thread_5 = NULL;



int idx__5__5 = 0;
void save_file_pointer__5(object_write_buffer *buf);
void load_file_pointer__5(object_write_buffer *buf);

inline void check_status__5() {
  check_thread_status(__state_flag_5, __thread_5);
}

void check_status_during_io__5() {
  check_thread_status_during_io(__state_flag_5, __thread_5);
}

void __init_thread_info_5(thread_info *info) {
  __state_flag_5 = info->get_state_flag();
}

thread_info *__get_thread_info_5() {
  if (__thread_5 != NULL) return __thread_5;
  __thread_5 = new thread_info(5, check_status_during_io__5);
  __init_thread_info_5(__thread_5);
  return __thread_5;
}

void __declare_sockets_5() {
  init_instance::add_outgoing(5,2, DATA_SOCKET);
}

void __init_sockets_5(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_5() {
}

void __peek_sockets_5() {
}

 
void init_VectSource__9_11__5();
inline void check_status__5();

void work_VectSource__9_11__5(int);



inline void __push_5_2(int data) {
BUFFER_5_2[HEAD_5_2]=data;
HEAD_5_2++;
}



 
void init_VectSource__9_11__5(){
}
void save_file_pointer__5(object_write_buffer *buf) {}
void load_file_pointer__5(object_write_buffer *buf) {}
 
void work_VectSource__9_11__5(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int __tmp3__8 = 0;/* int */

      // mark begin: SIRFilter VectSource

      (__tmp3__8 = (((idx__5__5)++) % 10))/*int*/;
      __push_5_2(__tmp3__8);
      // mark end: SIRFilter VectSource

    }
  }
}

