#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <message.h>
#include <netsocket.h>
#include <node_server.h>
#include <init_instance.h>
#include <master_server.h>
#include <save_state.h>
#include <save_manager.h>
#include <delete_chkpts.h>
#include <object_write_buffer.h>
#include <read_setup.h>
#include <ccp.h>
#include <timer.h>
#include <streamit_random.h>
#include "structs.h"
#include "fusion.h"

int __max_iteration;
int __timer_enabled = 0;
int __frequency_of_chkpts;
volatile int __vol;
proc_timer tt("total runtime");


int BUFFER_1_2[__BUF_SIZE_MASK_1_2 + 1];
int HEAD_1_2 = 0;
int TAIL_1_2 = 0;
int BUFFER_2_3[__BUF_SIZE_MASK_2_3 + 1];
int HEAD_2_3 = 0;
int TAIL_2_3 = 0;
int BUFFER_3_4[__BUF_SIZE_MASK_3_4 + 1];
int HEAD_3_4 = 0;
int TAIL_3_4 = 0;
int BUFFER_5_2[__BUF_SIZE_MASK_5_2 + 1];
int HEAD_5_2 = 0;
int TAIL_5_2 = 0;
extern void __splitter_0_work(int);
extern void init_VectSource__4_10__1();
extern void work_VectSource__4_10__1(int);
extern void __joiner_2_work(int);
extern void init_VectAddKernel__15_12__3();
extern void work_VectAddKernel__15_12__3(int);
extern void init_VectPrinter__19_13__4();
extern void work_VectPrinter__19_13__4(int);
extern void init_VectSource__9_11__5();
extern void work_VectSource__9_11__5(int);

int main(int argc, char **argv) {
  read_setup::read_setup_file();
  __max_iteration = read_setup::max_iteration;
  for (int a = 1; a < argc; a++) {
    if (argc > a + 1 && strcmp(argv[a], "-i") == 0) {
      int tmp;
      sscanf(argv[a + 1], "%d", &tmp);
#ifdef VERBOSE
      fprintf(stderr,"Number of Iterations: %d\n", tmp);
#endif
      __max_iteration = tmp;
    }
    if (strcmp(argv[a], "-t") == 0) {
#ifdef VERBOSE
      fprintf(stderr,"Timer enabled.\n");
#endif
      __timer_enabled = 1;
    }
  }
// number of phases: 5


  // ============= Initialization =============

init_VectSource__4_10__1();
init_VectSource__9_11__5();
init_VectAddKernel__15_12__3();
init_VectPrinter__19_13__4();

  // ============= Steady State =============

  if (__timer_enabled) {
    tt.start();
  }
  for (int n = 0; n < (__max_iteration  ); n++) {

HEAD_1_2 = 0;
TAIL_1_2 = 0;
    work_VectSource__4_10__1(1 );
HEAD_5_2 = 0;
TAIL_5_2 = 0;
    work_VectSource__9_11__5(1 );
HEAD_2_3 = 0;
TAIL_2_3 = 0;
    __joiner_2_work(1 );
HEAD_3_4 = 0;
TAIL_3_4 = 0;
    work_VectAddKernel__15_12__3(1 );
    work_VectPrinter__19_13__4(1 );
  }
if (__timer_enabled) {
    tt.stop();
    tt.output(stderr);
  }


  return 0;
}
