import java.io.Serializable;
import streamit.library.*;
import streamit.library.io.*;
import streamit.misc.StreamItRandom;
class Complex extends Structure implements Serializable {
  float real;
  float imag;
}
class float2 extends Structure implements Serializable {
  float x;
  float y;
}
class float3 extends Structure implements Serializable {
  float x;
  float y;
  float z;
}
class float4 extends Structure implements Serializable {
  float x;
  float y;
  float z;
  float w;
}
class StreamItVectorLib {
  public static native float2 add2(float2 a, float2 b);
  public static native float3 add3(float3 a, float3 b);
  public static native float4 add4(float4 a, float4 b);
  public static native float2 sub2(float2 a, float2 b);
  public static native float3 sub3(float3 a, float3 b);
  public static native float4 sub4(float4 a, float4 b);
  public static native float2 mul2(float2 a, float2 b);
  public static native float3 mul3(float3 a, float3 b);
  public static native float4 mul4(float4 a, float4 b);
  public static native float2 div2(float2 a, float2 b);
  public static native float3 div3(float3 a, float3 b);
  public static native float4 div4(float4 a, float4 b);
  public static native float2 addScalar2(float2 a, float b);
  public static native float3 addScalar3(float3 a, float b);
  public static native float4 addScalar4(float4 a, float b);
  public static native float2 subScalar2(float2 a, float b);
  public static native float3 subScalar3(float3 a, float b);
  public static native float4 subScalar4(float4 a, float b);
  public static native float2 scale2(float2 a, float b);
  public static native float3 scale3(float3 a, float b);
  public static native float4 scale4(float4 a, float b);
  public static native float2 scaleInv2(float2 a, float b);
  public static native float3 scaleInv3(float3 a, float b);
  public static native float4 scaleInv4(float4 a, float b);
  public static native float sqrtDist2(float2 a, float2 b);
  public static native float sqrtDist3(float3 a, float3 b);
  public static native float sqrtDist4(float4 a, float4 b);
  public static native float dot3(float3 a, float3 b);
  public static native float3 cross3(float3 a, float3 b);
  public static native float2 max2(float2 a, float2 b);
  public static native float3 max3(float3 a, float3 b);
  public static native float2 min2(float2 a, float2 b);
  public static native float3 min3(float3 a, float3 b);
  public static native float2 neg2(float2 a);
  public static native float3 neg3(float3 a);
  public static native float4 neg4(float4 a);
  public static native float2 floor2(float2 a);
  public static native float3 floor3(float3 a);
  public static native float4 floor4(float4 a);
  public static native float2 normalize2(float2 a);
  public static native float3 normalize3(float3 a);
  public static native float4 normalize4(float4 a);
  public static native boolean greaterThan3(float3 a, float3 b);
  public static native boolean lessThan3(float3 a, float3 b);
  public static native boolean equals3(float3 a, float3 b);
}
class IntSource extends Filter // HelloWorld.str:6
{
  public IntSource()
  {
    setStateful(true);
  }
  int x; // HelloWorld.str:7
  public void work() { // HelloWorld.str:11
    outputChannel.pushInt(x++); // HelloWorld.str:12
  }
  public void init() { // HelloWorld.str:8
    setIOTypes(Void.TYPE, Integer.TYPE); // HelloWorld.str:6
    addSteadyPhase(0, 0, 1, "work"); // HelloWorld.str:11
    x = 0; // HelloWorld.str:9
  }
}
class IntPrinter extends Filter // HelloWorld.str:16
{
  public IntPrinter()
  {
  }
  public void work() { // HelloWorld.str:17
    System.out.println(inputChannel.popInt()); // HelloWorld.str:18
  }
  public void init() { // HelloWorld.str:16
    setIOTypes(Integer.TYPE, Void.TYPE); // HelloWorld.str:16
    addSteadyPhase(1, 1, 0, "work"); // HelloWorld.str:17
  }
}
public class HelloWorld extends StreamItPipeline // HelloWorld.str:22
{
  public void init() { // HelloWorld.str:22
    add(new IntSource()); // HelloWorld.str:23
    add(new IntPrinter()); // HelloWorld.str:24
  }
}
