#!/bin/sh
SFG=sfg
DOT=stream-graph.dot
STRS=`find applications benchmarks -type f -name *.str`

mkdir -p $SFG
for str in $STRS
do
	echo 'processing' $str '...'
	path="${str%/*}"
	src="${str##*/}"

	cwd=`pwd`
	cd $path
	strc $src
	echo 'convert to' ${src%.str}.png '...'
	dot -Tpng -o ${src%.str}.png $DOT
	cd $cwd

	mkdir -p $SFG/$path
	cp $path/${src%.str}.png $SFG/$path
done
