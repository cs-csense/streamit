package streamit.frontend.nodes;

import java.util.List;
import java.util.Collections;

public class Annotation extends FENode
{
    private String OP;
    private List<Object> args;

    public Annotation(FEContext context, String OP, List<Object> args)
    {
        super(context);
        // TODO: check for validity, including types of object
        // in the lists and that all three are the same length.
        this.OP = OP;
				this.args = new java.util.ArrayList<Object>(args);
    }

    public String op()
    {
        return OP;
    }

    public Object arg(int n)
    {
        return args.get(n);
    }
    
    /** Accept a front-end visitor. */
    public Object accept(FEVisitor v)
    {
        return v.visitAnnotation(this);
    }

		@Override
		public String toString() {
				return String.format("(%s, %s)", OP, args);
		}
}
